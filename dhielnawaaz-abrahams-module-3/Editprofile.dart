import 'package:flutter/material.dart';
import 'package:saphca/dashboard.dart';

void main() {
  runApp(Editpro());
}

class Editpro extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
  const Editpro({Key? key}) : super(key: key);
}

class _MyAppState extends State<Editpro> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text('Edit Profile'),
          ),
          body: Center(
              child: Column(children: <Widget>[
            Container(
              margin: EdgeInsets.all(10),
              child: Tooltip(
                  message: 'My Account',
                  child: FloatingActionButton(
                    child: Icon(
                      Icons.account_box,
                      size: 50,
                    ),
                    onPressed: () {
                      print('You pressed the button.');
                    },
                  )),
            ),
            Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.all(10),
                child: const Text(
                  'Menu',
                  style: TextStyle(fontSize: 20),
                )),
            TextButton(
              onPressed: () {},
              child: const Text(
                'Current patient information : AAAA BBBB',
              ),
            ),
            Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.all(10),
                child: const Text(
                  'Edit your profile by updating current details',
                  style: TextStyle(fontSize: 20),
                )),
            Container(
                alignment: Alignment.topLeft,
                padding: const EdgeInsets.all(10),
                child: const Text(
                  'Current Patient Name: AAAA',
                  style: TextStyle(fontSize: 15),
                )),
            Container(
              padding: const EdgeInsets.all(10),
              child: TextField(
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Update Patient Name',
                ),
              ),
            ),
            Container(
                alignment: Alignment.topLeft,
                padding: const EdgeInsets.all(10),
                child: const Text(
                  'Current Patient Surname: BBBB',
                  style: TextStyle(fontSize: 15),
                )),
            Container(
              padding: const EdgeInsets.all(10),
              child: TextField(
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: ' Update Patient Surname',
                ),
              ),
            ),
            Container(
                alignment: Alignment.topLeft,
                padding: const EdgeInsets.all(10),
                child: const Text(
                  'Current Patient contact number: 000 000 0000',
                  style: TextStyle(fontSize: 15),
                )),
            Container(
              padding: const EdgeInsets.all(10),
              child: TextField(
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Update Patient Contact Number',
                ),
              ),
            ),
            Container(
                alignment: Alignment.topLeft,
                padding: const EdgeInsets.all(10),
                child: const Text(
                  'Current Patient Address: 00 A Road, B Town, 01234',
                  style: TextStyle(fontSize: 15),
                )),
            Container(
              padding: const EdgeInsets.all(10),
              child: TextField(
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Update Patient Address',
                ),
              ),
            ),
            TextButton(
              onPressed: () {},
              child: const Text(
                'Profile Update Complete',
              ),
            ),
            Container(
                height: 50,
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: ElevatedButton(
                  child: const Text('Save Profile'),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const Dashboard()),
                    );
                  },
                )),
          ]))),
    );
  }
}
