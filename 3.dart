class Appinfo {
  String? Appname;
  String? sector;
  String? developer;
  int? year;

  void printAppInfo() {
    Appname = Appname?.toUpperCase();
    print("The name of the App is $Appname");
    print("The App belongs  to the $sector sector(s)");
    print("The Apps developer is $developer");
    print("$year is the year the App won MTN Business App of the Year Awards");
  }
}

void main() {
  var AmbaniAfrica = new Appinfo();
  AmbaniAfrica.Appname = "Ambani Africa";
  AmbaniAfrica.sector =
      "Best South African solution, Best gaming solution and Best educational solution";
  AmbaniAfrica.developer = "Mukundi Lambani";
  AmbaniAfrica.year = 2021;

  AmbaniAfrica.printAppInfo();
}
